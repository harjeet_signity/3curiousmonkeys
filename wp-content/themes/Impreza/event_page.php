<?php
define('THEME_TEMPLATE', TRUE);
define('IS_FULLWIDTH', TRUE);
global $smof_data;
/* Template Name: Event Template */ 
get_header(); ?>
<?php if (have_posts()) { while(have_posts()) { the_post();
	global $us_page_id;
	$us_page_id = get_the_ID();
	?>
	<!-- NAV -->
	
		<!-- /NAV -->
	<?php get_template_part( 'templates/pagehead' ); ?>
    <div class="learn-slider-sec">
        <?php echo do_shortcode('[rev_slider learn_slider]'); ?>
    </div>
	<div class="learn-menu">
		<div class="container">
			<nav class="w-nav layout_hor">
					<ul class="w-nav-list level_1" style="display: block;">
						<?php wp_nav_menu(
							array(
								'theme_location' => 'internal_page_menu',
								'container'       => 'ul',
								'menu'            => 'Internal page menu',
								'container_class' => 'w-nav-list',
								'walker' => new Walker_Nav_Menu_us(),
								'items_wrap' => '%3$s',
								'fallback_cb' => false,

							));
						?>
					</ul>
			</nav>
		</div>
	</div>
	
<div class="learn-menu-title green-bg">
    <div class="container">
        <?php
		 if(get_field( "subtitle", $us_page_id)) :
			echo get_field( "subtitle", $us_page_id);
		 endif;
		?>
    </div>
</div>

<div class="learn-content-wrap event-wrap">
	<div class="container">
	<?php
		$i = 1;
		$new = new WP_Query('post_type=events&showposts=12&orderby=date');
		while ($new->have_posts()) : $new->the_post();
		  $event_date	=	 get_field('event_date', $new->post->ID);
		  if(($i%2) == '0') : ?>
		  <div class="row even">
                  <div class="col-md-4">
                      <?php
                                                   if ( has_post_thumbnail() ) {
                                                       $image_url = wp_get_attachment_image_src( get_post_thumbnail_id($new->post->ID ), 'full');
                      ?>
                      <img width="360" height="265" src="<?php echo $image_url[0] ;?>"/>
                      <?php
                                                   }
                      ?>
                  </div>
				<div class="col-md-8">
					<h4>
						<?php
						if(!empty($event_date)) :
						$event_time	=	 get_field('event_time', $new->post->ID);
						echo date('F j, Y',strtotime($event_date)).'&nbsp;&nbsp;'.$event_time ;
						//echo $event_date;
						endif ;
						?>
					</h4>
					<h3><?php the_title(); ?></h3> 
					<?php the_content() ;?>
				</div>
				
			</div>
			<?php
			else :
			?>
			<div class="row odd">
				<div class="col-md-4">
					<?php
					if ( has_post_thumbnail() ) {
						$image_url = wp_get_attachment_image_src( get_post_thumbnail_id($new->post->ID ), 'full');
					?>
                    <img width="360" height="265" src="<?php echo $image_url[0] ;?>"/>
					<?php
					}
					?>
				</div>
				<div class="col-md-8">
					<h4>
						<?php
						if(!empty($event_date)) :
						$event_time	=	 get_field('event_time', $new->post->ID);
						echo date('F j, Y',strtotime($event_date)).'&nbsp;&nbsp;'.$event_time ;
						//echo $event_date;
						endif ;
						?>
					</h4>
					<h3><?php the_title(); ?></h3> 
					<?php the_content() ;?>
				</div>
			</div>
			<?php
			endif;
		
	$i++;
	endwhile;
	?>
	</div>
</div>
	
<?php }  } else { ?>
	<?php _e('No posts were found.', 'us'); ?>
<?php } ?>
<?php get_footer(); ?>
