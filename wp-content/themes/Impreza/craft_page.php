<?php
define('THEME_TEMPLATE', TRUE);
define('IS_FULLWIDTH', TRUE);
global $smof_data;
/* Template Name: Crafts Template */ 
get_header(); ?>
<?php if (have_posts()) { while(have_posts()) { the_post();
	global $us_page_id;
	$us_page_id = get_the_ID();
	?>
	<!-- NAV -->
	
		<!-- /NAV -->
	<?php get_template_part( 'templates/pagehead' ); ?>
    <div class="learn-slider-sec">
        <?php echo do_shortcode('[rev_slider learn_slider]'); ?>
    </div>
	<div class="learn-menu">
		<div class="container">
			<nav class="w-nav layout_hor">
					<ul class="w-nav-list level_1" style="display: block;">
						<?php wp_nav_menu(
							array(
								'theme_location' => 'internal_page_menu',
								'container'       => 'ul',
								'menu'            => 'Internal page menu',
								'container_class' => 'w-nav-list',
								'walker' => new Walker_Nav_Menu_us(),
								'items_wrap' => '%3$s',
								'fallback_cb' => false,

							));
						?>
					</ul>
			</nav>
		</div>
	</div>
	
<div class="learn-menu-title purple-bg">
    <div class="container">
        <?php
		 if(get_field( "subtitle", $us_page_id)) :
			echo get_field( "subtitle", $us_page_id);
		 endif;
		?>
    </div>
</div>

<div class="learn-content-wrap craft-wrap">
	<div class="container">
	<?php
		$i = 1;
		$new = new WP_Query('post_type=crafts&showposts=12&orderby=date');
		while ($new->have_posts()) : $new->the_post();
			if(($i%2) == '0') : ?>
			<div class="row even">
                <div class="col-md-4">
                    <?php
					$selected	=	 get_field('image_or_video', $new->post->ID);
					if($selected == 'Image'){
						if ( has_post_thumbnail() ) {
							$image_url = wp_get_attachment_image_src( get_post_thumbnail_id($new->post->ID ), 'full');
						?>
							<img width="360" height="265" src="<?php echo $image_url[0] ;?>"/>
						<?php
						}
					} else {
						$post_id		=	get_the_ID();								
						$video_url		= get_field( "video_url", $post_id );								
						$video_url		=	trim($video_url, ' ');
						if(!empty($video_url)) {
							$video_url_expolded	=	explode("/",$video_url);	
							$video_url_last		=	end($video_url_expolded);	
							$embed_frame	=	'<iframe width="100%" height="265" src="https://www.youtube.com/embed/'.$video_url_last.'" frameborder="0" allowfullscreen></iframe>';
							$embed_frame	=	'<iframe width="100%" height="265" src="https://www.youtube.com/embed/'.$video_url_last.'" frameborder="0" allowfullscreen></iframe>';
							
						} else {
							$embed_frame	=	'<h3>Sorry! Video not availabe.</h3>';
						}
						echo $embed_frame;
					}                              
                                                 
                    
                                               
                    ?>
                </div>
				<div class="col-md-8">
					<h3><?php the_title(); ?></h3> 
					<?php the_content() ;?>
				</div>
			</div>
			<?php
			else :
			?>
			<div class="row odd">
				<div class="col-md-4">
					<?php
					$selected	=	 get_field('image_or_video', $new->post->ID);
					if($selected == 'Image'){
						if ( has_post_thumbnail() ) {
							$image_url = wp_get_attachment_image_src( get_post_thumbnail_id($new->post->ID ), 'full');
						?>
							<img width="360" height="265" src="<?php echo $image_url[0] ;?>"/>
						<?php
						}
					} else {
						$post_id		=	get_the_ID();								
						$video_url		= get_field( "video_url", $post_id );								
						$video_url		=	trim($video_url, ' ');
						if(!empty($video_url)) {
							$video_url_expolded	=	explode("/",$video_url);	
							$video_url_last		=	end($video_url_expolded);	
							$embed_frame	=	'<iframe width="100%" height="265" src="https://www.youtube.com/embed/'.$video_url_last.'" frameborder="0" allowfullscreen></iframe>';
							$embed_frame	=	'<iframe width="100%" height="265" src="https://www.youtube.com/embed/'.$video_url_last.'" frameborder="0" allowfullscreen></iframe>';
							
						} else {
							$embed_frame	=	'<h3>Sorry! Video not availabe.</h3>';
						}
						echo $embed_frame;
					} 
					?>
				</div>
				<div class="col-md-8">
					<h3><?php the_title(); ?></h3> 
					<?php the_content() ;?>
				</div>
			</div>
			<?php
			endif;
		
	$i++;
	endwhile;
	?>
	</div>
</div>

	
<?php }  } else { ?>
	<?php _e('No posts were found.', 'us'); ?>
<?php } ?>
<?php get_footer(); ?>
