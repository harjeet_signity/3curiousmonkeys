<?php
define('THEME_TEMPLATE', TRUE);
define('IS_FULLWIDTH', TRUE);
global $smof_data;
/* Template Name: Press Template */ 
get_header(); ?>
 <?php
	global $us_page_id;
	$us_page_id = get_the_ID();
	?>
	<!-- NAV -->
	
		<!-- /NAV -->
	<?php get_template_part( 'templates/pagehead' ); ?>
    <div class="press-slider-sec">
        <?php echo do_shortcode('[rev_slider press]'); ?>
    </div>


<div class="press-content-wrap blog_section">
	<div class="container">
	<?php
		$paged 			=	(get_query_var('paged')) ? get_query_var('paged') : 1	;
		$limit 			= 	get_option('posts_per_page');	
		query_posts('post_type=pressblogs&posts_per_page='.$limit.'&paged='.$paged .'&orderby=date');
		while (have_posts()) : the_post();
		$post_id 		= 	get_the_ID();
			 ?>
			<div class="row press_blog">
                <div class="col-md-3 col-sm-3">
                    <?php
						$media_logo	=	 get_field('media_logo', $post_id );
						$news_url	=	 get_field('news_url', $post_id );  
						if(!empty($media_logo)) {
						?>
							<img src="<?php echo $media_logo ;?>"/>
						<?php
						}  
                    ?>
                    
                </div>
                <div class="col-md-9 col-sm-9">
                    <h4><?php the_title(); ?></h4>
                    <?php 
                    if(get_field('show_date', $post_id )){
						echo get_the_date();
					}
					the_content() ;
						if(!empty($news_url)) {
						?>
                        <a href="<?php echo $news_url; ?>" target="_blank">Read More <i class="fa fa-angle-double-right"></i></a>
						<?php
					} 
					?>
				</div>
			</div>
			
			<?php
	
	endwhile;
	?>
	<br/>
	<div class="navigationgernator_class pull-right"><?php wp_pagenavi();?></div>
	</div>
</div>


<?php get_footer(); ?>
