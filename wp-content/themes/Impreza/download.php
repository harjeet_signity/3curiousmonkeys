<?php	
	// define the path to your download folder plus assign the file name
	$path 		= $_GET['path'];
	$filename	= $_GET['f_name'];
	
	// get the file size and send the http headers
	$size = filesize($path);
	header('Content-Type: application/octet-stream');
	header('Content-Length: '.$size);
	header('Content-Disposition: attachment; filename='.$filename);
	header('Content-Transfer-Encoding: binary');
	// open the file in binary read-only mode
	// display the error messages if the file can´t be opened
	$file = @ fopen($path, 'rb');
	
	if ($file) {
		// stream the file and exit the script when complete
		fpassthru($file);
		exit;
	} 
?>
