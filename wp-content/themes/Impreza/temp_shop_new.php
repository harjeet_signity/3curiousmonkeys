<?php
define('THEME_TEMPLATE', TRUE);
define('IS_FULLWIDTH', TRUE);
global $smof_data;
/* Template Name: Shop New Template */ 
get_header(); ?>
<?php if (have_posts()) { while(have_posts()) { the_post();
	global $us_page_id;
	$us_page_id = get_the_ID();
	?>
	<?php get_template_part( 'templates/pagehead' ); ?>
    <div class="learn-slider-sec">
        <?php echo do_shortcode('[rev_slider learn_slider]'); ?>
    </div><!--
	<div class="learn-menu">
		<div class="container">
			<nav class="w-nav layout_hor">
					<ul class="w-nav-list level_1" style="display: block;">
						<?php wp_nav_menu(
							array(
								'theme_location' => 'internal_page_menu',
								'container'       => 'ul',
								'menu'            => 'Internal page menu',
								'container_class' => 'w-nav-list',
								'walker' => new Walker_Nav_Menu_us(),
								'items_wrap' => '%3$s',
								'fallback_cb' => false,

							));
						?>
					</ul>
			</nav>
		</div>
	</div>
	<div class="learn-menu-title pink-bg">
		<div class="container">
			<?php
			 if(get_field( "subtitle", $us_page_id)) :
				echo get_field( "subtitle", $us_page_id);
			 endif;
			?>
			
		</div>
	</div>-->


<?php }  } else { ?>
	<?php _e('No posts were found.', 'us'); ?>
<?php } ?>
<div class="learn-content-wrap activities-wrap">
	<div class="container">            
		<?php
			$new 				=	new WP_Query('post_type=shop_products&showposts=9&orderby=date');
			$fetched_posts		=	$new->found_posts;
			$i					=	0;
			while ($new->have_posts()) : $new->the_post();
			if($i == 0) {
				echo "<div class='row'>";								
			} elseif (($i%3 == 0) AND ($i!=$fetched_posts)){
				echo "</div><div class='row'>";
			} elseif ($i == $fetched_posts) {
				echo "</div>";
			
				break;
			}
		?>
			
				<div class="col-md-4">
					<h3><?php echo the_title(); ?></h3>
					<?php 
					if(get_field('product_image', $new->post->ID)) :
						$img_url		=	 get_field('product_image', $new->post->ID) ;
						?>
						<img src="<?php echo $img_url ;?>" class="img-responsive productimg"/> <br>
						<?php
					endif;
					?>
										
					<?php
					if(get_field('buy_link', $new->post->ID)) :
						$buy_link		=	 get_field('buy_link', $new->post->ID);	
						
					?>
					<a href="<?php echo $buy_link ;?>" class="downloadactivity" target="_blank">Buy Now <i class="fa fa-angle-double-right"></i></a>
					<?php
					else :
					?>
					<a href="javascript:;" class="downloadactivity">Buy Now <i class="fa fa-angle-double-right"></i></a>
					<?php
					
					endif;
					?>
					
				</div>
		<?php
		$i++;
		
		endwhile;
		?> 
			           
	</div>
	<?php
		if(get_field('cafe_press', $us_page_id)){
			$press_link		=	get_field('cafe_press', $us_page_id);
			?>
			<div class="row" style="text-align:center;">
				<a href='<?php echo $press_link; ?>' class="cafepress" target="_blank">Visit our cafe press shop >></a>
			</div>
			<?php
		} else {
			?>
			
			<div class="row" style="text-align:center;">
				<a href='javascript:;' class="cafepress">Visit our cafe press shop >></a>
			</div>
			<?php
			
		}
	?>
	
</div>
<?php
if (have_posts()) { while(have_posts()) { the_post(); 
?>
<div class="learn-content-wrap container_spc">
	
		<?php the_content(); ?>
	
</div>


<?php
} }
?>
<?php get_footer(); ?>
