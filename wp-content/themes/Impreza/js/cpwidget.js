/*
 * CPWidget v2.0 by ianmassey
 * 
 * Dependencies: jQuery
 *
 * Input API:
 * Call CPWidget.init() passing an object containing the following:
 * 		title 		(50 chars max)
 *		topcolor	(hex color value)
 *		bottomcolor	(hex color value)
 *		bgcolor 	(hex color value)
 *		rows		(1-5)
 *		cols 		(1-5)
 *		imgsize		(125, 200, 250, or 300)
 *		prodtitle 	(true or false)
 *		partnerid	(CP affiliate ID)
 *		shopid		(CP shop id)
 *		tags		(comma separated list of tags)
 *		
 */

jQuery(function($){
	(function(doc, win, $, undefined){

		// base class
		CPWidget = {

			// root element
			root: $('#cpwidget'),

			// error counter for rows of products
			rowErrorCount: 0,

			// initialize widget
			init: function(args){

				var i;

				this.args = args;

				// load stylesheet
				if( !doc.getElementById('cpwidget-css') ){
					$('head').append('<link rel="stylesheet" type="text/css" id="cpwidget-css" href="http://content.cpcache.com/cplabs/cpwidget.css?v=4" media="all"/>');
				}

				// build markup
				var seeAllLink = "", // TODO: populate link depending on how products are chosen
					markup = '<h4>' + args.title + '</h4>\
								<div id="cpwidget-titlebar">\
									<a href="http://www.cafepress.com?utm_medium=affiliate&utm_campaign=cpwidget" title="CafePress" target="_blank">\
										<img src="http://content.cpcache.com/cplabs/widget_logo.png" alt="CafePress"/>\
									</a>\
									<a id="cpwidget-see-all" href="' + seeAllLink + '" title="See All" target="_blank">\
										<img src="http://content.cpcache.com/cplabs/widget_see_all.png" alt="See All"/>\
									</a>\
								</div>\
								<div id="cpwidget-container"></div>';

				// render widget
				this.root.append( $(markup) );

				// insert carousel(s)
				$('#cpwidget-container').append( this.buildLists() );

				// apply user-supplied colors
				$('#cpwidget')
					.css('background-color', '' + args.bgcolor)
					.find('#cpwidget-titlebar')
						.css('background', '' + args.topcolor)
						.css('background', '-moz-linear-gradient(top, ' + args.topcolor + ' 0%, ' + args.bottomcolor + ' 100%)')
						.css('background', '-webkit-gradient(linear, left top, left bottom, color-stop(0%,' + args.topcolor + '), color-stop(100%,' + args.bottomcolor + ')')
						.css('background', '-webkit-linear-gradient(top, ' + args.topcolor + ' 0%,' + args.bottomcolor + ' 100%)')
						.css('background', '-o-linear-gradient(top, ' + args.topcolor + ' 0%,' + args.bottomcolor + ' 100%)')
						.css('background', '-ms-linear-gradient(top, ' + args.topcolor + ' 0%,' + args.bottomcolor + ' 100%)')
						.css('background', 'linear-gradient(to bottom, ' + args.topcolor + ' 0%,' + args.bottomcolor + ' 100%)')
						.css('filter', 'progid:DXImageTransform.Microsoft.gradient( startColorstr="' + args.topcolor + '", endColorstr="' + args.bottomcolor + '",GradientType=0 )');

				// get product list, insert into widget
				for( i = 0; i < args.rows; i++){
					$('.cpwidget-row').eq(i).append( this.fetchProducts(args, i) );
				}

				// initialize carousel
				this.initCarousel();
			},


			// build markup for carousel lists
			buildLists: function(){
				var lists = "",
					i;

				// build out lists for rows
				for ( i = 0; i < this.args.rows; i++ ){
					lists += '<ul style="height:' + this.args.imgsize + 'px;" class="cpwidget-row"></ul>';
				}

				return $(lists);
			},


			// build markup for individual row from api response data
			buildList: function(data){
				var markup = '',
					i,
					arr,
					thumbnailUrl,
					affiliateParam = '',
					utm_params = 'utm_medium=affiliate&utm_campaign=cpwidget';

				if('searchResultSet' in data){
					// add affiliate ID if supplied
					if( this.args.partnerid !== "" ){
						affiliateParam = '?aid=' + this.args.partnerid + '&' + utm_params;
					} else {
						affiliateParam = '?' + utm_params;
					}
					// handle tag search and "top" list
					for( i = 0; i < data.searchResultSet.mainResults.searchResultItem.length; i++ ){
						arr = data.searchResultSet.mainResults.searchResultItem[i];
						thumbnailUrl = arr.products.product.thumbnailUrl.replace(/height=([0-9]+)&width=([0-9]+)/, 'height=' + this.args.imgsize + '&width=' + this.args.imgsize);
						markup += '<li style="height:' + this.args.imgsize + 'px;width:' + (parseInt(this.args.imgsize, 10) + 50) + 'px;"><a target="_blank" href="' + arr.marketplaceUrl + affiliateParam + '"><img src="' + thumbnailUrl + '"/></a>';
						if( this.args.prodtitle == "true" ){
							markup += '<div class="cpwidget-prod-title-bg" style="width:' + this.args.imgsize + 'px;"><a class="cpwidget-prod-title" target="_blank" href="' + arr.marketplaceUrl + affiliateParam + '">' + arr.products.product.caption + '</a></div></li>';
						} else {
							markup += '</li>';
						}
					}
				} else if( 'products' in data && data.products !== null ){
					// handle product list by store
					for( i = 0; i < data.products.product.length; i++ ){
						arr = data.products.product[i];
						thumbnailUrl = arr.defaultProductUri.replace(/height=([0-9]+)&width=([0-9]+)/, 'height=' + this.args.imgsize + '&width=' + this.args.imgsize);
						markup += '<li style="height:' + this.args.imgsize + 'px;width:' + (parseInt(this.args.imgsize, 10) + 50) + 'px;"><a target="_blank" href="' + arr.marketplaceUri + '?' + utm_params + '"><img src="' + thumbnailUrl + '"/></a>';
						if( this.args.prodtitle == "true" ){
							markup += '<div class="cpwidget-prod-title-bg" style="width:' + this.args.imgsize + 'px;"><a class="cpwidget-prod-title" target="_blank" href="' + arr.marketplaceUri + '?' + utm_params + '">' + arr.name + '</a></div></li>';
						} else {
							markup += '</li>';
						}
					}
				} else {
					// we will check against this later to see if there were errors when we fetched products
					this.rowErrorCount += 1;
				}

				return markup;
			},


			// get product list for a given row
			fetchProducts: function(args, rowNum){
				var self = this,
					data,
					url,
					utm_params = 'utm_medium=affiliate&utm_campaign=cpwidget';

				if( args.shopid !== '' ){
					// pull products from shop
					url = '/search.json?shopid=' + args.shopid;
					url += '&pageNumber=' + (rowNum + 1);
					url += '&' + utm_params;
					// remove SEE ALL link for shop widget
					$('#cpwidget-see-all').remove();
					$.ajax({
						url: url,
						success: function(response, status, xhr){
							if( response.error ){
								alert(response.error);
							} else {
								$('.cpwidget-row').eq(rowNum).append(self.buildList(response));
								// set row width
								$('.cpwidget-row').eq(rowNum).each(function(){
									$(this).css('width', ( (parseInt(self.args.imgsize, 10) + 50) * $(this).find('li').length ) + 'px');
								});
								// handling this error kind of lamely here, as it is impractical to do so inside the builder
								if( (rowNum + 1) == args.rows && self.rowErrorCount > 0 ){
									if( self.rowErrorCount < args.rows ){
										alert("The shop belonging to " + self.args.shopid + " does not have enough products in it.  Please reduce the number of rows in your widget.");
									} else {
										alert("The shop belonging to " + self.args.shopid + " is either invalid or contains no products.");
									}
								}
							}
						},
						error: function(xhr, status, err){
							console.log('error', err);
						}
					});
				} else {
					// pull products by tag OR top products
					url = '/search.json?query=' + args.tags.replace(' ', '%20');
					url += '&pageNumber=' + (rowNum + 1);
					url += '&' + utm_params;
					// set SEE ALL link href
					$('#cpwidget-see-all').attr('href', 'http://www.cafepress.com/+' + args.tags.replace(/,?\s+/g, '-') + '?' + utm_params);
					$.ajax({
						url: url,
						success: function(response, status, xhr){
							if( 'mainResults' in response.searchResultSet && response.searchResultSet.mainResults !== null ){
								$('.cpwidget-row').eq(rowNum).append(self.buildList(response));
							} else {
								alert('Not enough products found for your search term(s).  Please use a comma separated list of tags.');
							}
							// set row width
							$('.cpwidget-row').eq(rowNum).each(function(){
								$(this).css('width', ( (parseInt(self.args.imgsize, 10) + 50) * $(this).find('li').length ) + 'px');
							});
						},
						error: function(xhr, status, err){
							console.log('error', err);
						}
					});
				}
			},


			// initialize carousel behaviors and styles
			initCarousel: function(){
				var self = this,
					i;

				// widget scrolling start point
				this.startingPoint = [];
				for( i = 0; i < this.args.rows; i++ ){
					this.startingPoint.push(0);
				}

				// update styles to reflect img size
				this.endingPoint = [];
				for( i = 0; i < this.args.rows; i++ ){
					this.endingPoint.push( (parseInt(self.args.imgsize, 10) + 50) * 25 );
				}

				// set overall widget width
				$('#cpwidget')
					.css('width', (parseInt(self.args.cols, 10) * (parseInt(self.args.imgsize, 10) + 50)) + 'px');

				// add carousel controls
				$('.cpwidget-row').each(function(i, e){
					var container = $(this).closest('#cpwidget-container'),
						topDistance;

					// make index correspond with row numbers
					i++;

					container.append('<a href="#" id="cpwidget-left-' + i + '" class="cpwidget-scroll-left cpwidget-scroll-left-inactive"></a>');
					container.append('<a href="#" id="cpwidget-right-' + i + '" class="cpwidget-scroll-right cpwidget-scroll-right-inactive"></a>');

					// your guess is as good as mine
					topDistance = Math.floor( ((parseInt( self.args.imgsize, 10) - 27) / 2) );
					topDistance *= (i + (i - 1));
					topDistance += ((i - 1) * 30);

					container.find('#cpwidget-left-' + i)
						.css('top', topDistance);
					container.find('#cpwidget-right-' + i)
						.css('top', topDistance);
				});

				// carousel navigation styles
				$('.cpwidget-scroll-left-inactive')
					.hover(function(){
						$(this)
							.toggleClass('cpwidget-scroll-left-active')
							.toggleClass('cpwidget-scroll-left-inactive');
					});
				$('.cpwidget-scroll-right-inactive')
					.hover(function(){
						$(this)
							.toggleClass('cpwidget-scroll-right-active')
							.toggleClass('cpwidget-scroll-right-inactive');
					});

				// carousel navigation behaviors
				$('.cpwidget-scroll-left')
					.click(function(e){
						e.preventDefault();
						self.carouselLeft( $(this) );
					});
				$('.cpwidget-scroll-right')
					.click(function(e){
						e.preventDefault();
						self.carouselRight( $(this) );
					});
			},


			// move carousel to the left
			carouselLeft: function(self){
				var container = self.closest('#cpwidget-container'),
					id = self.attr('id'),
					rowId = parseInt(id.substr(id.length - 1, 1), 10),
					sp = this.startingPoint[rowId - 1],
					row = container.find('.cpwidget-row').eq(rowId - 1),
					newLeftVal,
					outerWidth = row.find('li').outerWidth();

				if( sp == 0 ){
					return;
				} else {
					newLeftVal = sp + outerWidth;
					row.css('left', newLeftVal);
					this.startingPoint[rowId - 1] = newLeftVal;
				}
			},


			// move carousel to the right
			carouselRight: function(self){
				var container = self.closest('#cpwidget-container'),
					id = self.attr('id'),
					rowId = parseInt(id.substr(id.length - 1, 1), 10),
					sp = this.startingPoint[rowId - 1],
					row = container.find('.cpwidget-row').eq(rowId - 1),
					newLeftVal,
					outerWidth = row.find('li').outerWidth();

				//if( sp == -(this.endingPoint[rowId - 1] - outerWidth) ){
				if( Math.abs(sp) + (parseInt(this.args.cols, 10) * (parseInt(this.args.imgsize, 10) + 50)) >= row.outerWidth() ){
					return;
				} else {
					newLeftVal = sp - outerWidth;
					row.css('left', newLeftVal);
					this.startingPoint[rowId - 1] = newLeftVal;
				}
			}

		};

	})(document, window, jQuery);
});
