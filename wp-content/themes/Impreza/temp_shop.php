<?php
define('THEME_TEMPLATE', TRUE);
define('IS_FULLWIDTH', TRUE);
global $smof_data;
/* Template Name: Shop Template */ 
get_header(); ?>
<?php if (have_posts()) { while(have_posts()) { the_post();
	global $us_page_id;
	$us_page_id = get_the_ID();
	?>
	<?php get_template_part( 'templates/pagehead' ); ?>
    <div class="learn-slider-sec">
        <?php echo do_shortcode('[rev_slider learn_slider]'); ?>
    </div>
	<div class="learn-menu">
		<div class="container">
			<nav class="w-nav layout_hor">
					<ul class="w-nav-list level_1" style="display: block;">
						<?php wp_nav_menu(
							array(
								'theme_location' => 'internal_page_menu',
								'container'       => 'ul',
								'menu'            => 'Internal page menu',
								'container_class' => 'w-nav-list',
								'walker' => new Walker_Nav_Menu_us(),
								'items_wrap' => '%3$s',
								'fallback_cb' => false,

							));
						?>
					</ul>
			</nav>
		</div>
	</div>
<div class="learn-menu-title pink-bg">
    <div class="container">
		<?php
		 if(get_field( "subtitle", $us_page_id)) :
			echo get_field( "subtitle", $us_page_id);
		 endif;
		?>
		
    </div>
</div>

<div class="learn-content-wrap">
	<?php if (us_is_vc_fe()) { ?>
		<div class="l-submain">
			<div class="l-submain-h g-html i-cf">
				<?php the_content(); ?>
			</div>
		</div>
	<?php } else { ?>
		<?php the_content(); ?>
	<?php } ?>
	<?php if (@$smof_data['page_comments'] == 1 AND (comments_open() || get_comments_number() != '0') AND ( ! is_front_page())) { ?>
	<div class="l-submain for_comments color_alternate">
		<div class="l-submain-h g-html i-cf">
		<?php comments_template();?>
		</div>
	</div>
	<?php } ?>
</div>
<?php }  } else { ?>
	<?php _e('No posts were found.', 'us'); ?>
<?php } ?>
<?php get_footer(); ?>
