<?php
define('THEME_TEMPLATE', TRUE);
define('IS_FULLWIDTH', TRUE);
global $smof_data;
/* Template Name: Activity Template */ 
get_header(); ?>
<?php if (have_posts()) { while(have_posts()) { the_post();
	global $us_page_id;
	$us_page_id = get_the_ID();
	?>
	<!-- NAV -->
	
		<!-- /NAV -->
	<?php get_template_part( 'templates/pagehead' ); ?>
	
    <div class="learn-slider-sec">
        <?php echo do_shortcode('[rev_slider learn_slider]'); ?>
    </div>
	<div class="learn-menu">
		<div class="container">
			<nav class="w-nav layout_hor">
					<ul class="w-nav-list level_1" style="display: block;">
						<?php wp_nav_menu(
							array(
								'theme_location' => 'internal_page_menu',
								'container'       => 'ul',
								'menu'            => 'Internal page menu',
								'container_class' => 'w-nav-list',
								'walker' => new Walker_Nav_Menu_us(),
								'items_wrap' => '%3$s',
								'fallback_cb' => false,

							));
						?>
					</ul>
			</nav>
		</div>
	</div>
	
	
    <div class="learn-menu-title blue-bg">
        <div class="container">
            <?php
		 if(get_field( "subtitle", $us_page_id)) :
			echo get_field( "subtitle", $us_page_id);
		 endif;
		?>
        </div>
    </div>

    <div class="learn-content-wrap activities-wrap">
        <div class="container">            
			<?php
				$new 				=	new WP_Query('post_type=activities&showposts=12&orderby=date');
				$fetched_posts		=	$new->found_posts;
				$i					=	0;
				while ($new->have_posts()) : $new->the_post();
				if($i == 0) {
					echo "<div class='row'>";								
				} elseif (($i%3 == 0) AND ($i!=$fetched_posts)){
					echo "</div><div class='row'>";
				} elseif ($i == $fetched_posts) {
					echo "</div>";
					break;
				}
			?>
				
					<div class="col-md-4">
						<p><?php echo the_title(); ?></p>
						<?php 
						if(get_field('feature_image', $new->post->ID)) :
							$img_url		=	 get_field('feature_image', $new->post->ID) ;
							?>
							<img src="<?php echo $img_url ;?>" width="200"/> <br>
							<?php
						endif;
						
						if(get_field('upload_doc', $new->post->ID)) :
							$file_link		=	 get_field('upload_doc', $new->post->ID);	
							//echo $file_link.'<br>';							
							$file_path		=	 getcwd().end(explode('3cm',$file_link));
							//echo $file_path.'<br>';
							$filename		=	end(explode('/',$file_link));
							
						    $php_file_url	=	 get_template_directory_uri().'/download.php?path='.$file_path.'&f_name='.$filename;
														?>
						<a href="<?php echo $file_link ;?>" class="downloadactivity" download>
						<?php /* ?>
						<a href="<?php echo $php_file_url ;?>" class="downloadactivity">
						<?php */ ?>
						
						Download Now <i class="fa fa-angle-double-right"></i></a>
						<?php
						else :
						?>
						<a href="javascript:;" class="downloadactivity">Download Now <i class="fa fa-angle-double-right"></i></a>
						<?php
						endif;
						?>

					</div>
			<?php
			$i++;
			endwhile;
			?>            
        </div>
    </div>
	
<?php }  } else { ?>
	<?php _e('No posts were found.', 'us'); ?>
<?php } ?>
<?php get_footer(); ?>
