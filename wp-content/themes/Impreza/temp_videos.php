 <?php
define('THEME_TEMPLATE', TRUE);
define('IS_FULLWIDTH', TRUE);
global $smof_data;
/* Template Name: Videos Template */ 
get_header(); ?>
<?php if (have_posts()) { while(have_posts()) { the_post();
	global $us_page_id;
	$us_page_id = get_the_ID();
	?>
	<!-- NAV -->
	
		<!-- /NAV -->
	<?php get_template_part( 'templates/pagehead' ); ?>
	
    <div class="learn-slider-sec">
        <?php echo do_shortcode('[rev_slider learn_slider]'); ?>
    </div>
	<div class="learn-menu">
		<div class="container">
			<nav class="w-nav layout_hor">
					<ul class="w-nav-list level_1" style="display: block;">
						<?php wp_nav_menu(
							array(
								'theme_location' => 'internal_page_menu',
								'container'       => 'ul',
								'menu'            => 'Internal page menu',
								'container_class' => 'w-nav-list',
								'walker' => new Walker_Nav_Menu_us(),
								'items_wrap' => '%3$s',
								'fallback_cb' => false,

							));
						?>
					</ul>
			</nav>
		</div>
	</div>
	
<div class="learn-menu-title brown-bg">
    <div class="container">
		<?php
		 if(get_field( "subtitle", $us_page_id)) :
			echo get_field( "subtitle", $us_page_id);
		 endif;
		?>
        
    </div>
</div>

<div class="learn-content-wrap video-wrap">
	<div class="container">
		<?php
			$i = 1;
			$new = new WP_Query('post_type=videos&showposts=12&orderby=date');
			while ($new->have_posts()) : $new->the_post();
				if(($i%2) == '0') : ?>
				<div class="row even">
                    <div class="col-md-5">
                        <?php
							$post_id		=	get_the_ID();								
							$share_link_full 	=	get_field( "share_link", $post_id );								
							$share_link		=	trim($share_link_full, ' ');
							$share_link		=	explode('/', $share_link);
						
							if(!empty($share_link)){
								if(in_array("www.facebook.com", $share_link)){
									echo do_shortcode( '[fbvideo link="'.$share_link_full.'" width="360" height="265" onlyvideo="1"] ' );
								} else {
								   
								   $share_link_last		=	end($share_link);	
								   echo '<iframe width="360" height="265" src="https://www.youtube.com/embed/'.$share_link_last.'" frameborder="0" allowfullscreen></iframe>';
								   							   
								}								
							}  else {
							   echo '<h3>Sorry! Video not availabe.</h3>';
							}
                        ?>
                    </div>
                    
					<div class="col-md-7">
						<h3><?php the_title(); ?></h3> 
						<?php the_content() ;?>
					</div>
				</div>
				<?php
				else :
				?>
				<div class="row odd">
					<div class="col-md-5">
						<?php
							$post_id		=	get_the_ID();								
							$share_link_full 	=	get_field( "share_link", $post_id );								
							$share_link		=	trim($share_link_full, ' ');
							$share_link		=	explode('/', $share_link);
						
							if(!empty($share_link)){
								if(in_array("www.facebook.com", $share_link)){
									echo do_shortcode( '[fbvideo link="'.$share_link_full.'" width="360" height="265" onlyvideo="1"] ' );
								} else {
								   
								   $share_link_last		=	end($share_link);	
								   echo '<iframe width="360" height="265" src="https://www.youtube.com/embed/'.$share_link_last.'" frameborder="0" allowfullscreen></iframe>';
								   							   
								}								
							}  else {
							   echo '<h3>Sorry! Video not availabe.</h3>';
							}
						
						?>
					</div>
                    <div class="col-md-7">
                        <h3><?php the_title(); ?></h3> 
                        <?php the_content() ;?>
                    </div>
				</div>
				<?php
				endif;
			
		$i++;
		endwhile;
		?>
	</div>
</div>

	
<?php }  } else { ?>
	<?php _e('No posts were found.', 'us'); ?>
<?php } ?>
<?php get_footer(); ?>
