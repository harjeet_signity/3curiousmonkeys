<?php
/**
 * Include all needed files
 */

$template_directory = get_template_directory();
/* Slightly Modified Options Framework */
require_once ($template_directory.'/admin/index.php');
/* Admin specific functions */
require_once($template_directory.'/functions/admin.php');
/* Load shortcodes */
require_once($template_directory.'/functions/shortcodes.php');
require_once($template_directory.'/functions/zilla-shortcodes/zilla-shortcodes.php');
/* Breadcrumbs function */
require_once($template_directory.'/functions/breadcrumbs.php');
/* Post formats */
require_once($template_directory.'/functions/post_formats.php');
/* Custom Post types */
require_once($template_directory.'/functions/post_types.php');
/* Meta Box plugin and settings */
define('RWMB_URL', trailingslashit(get_template_directory_uri().'/vendor/meta-box'));
define('RWMB_DIR', trailingslashit($template_directory.'/vendor/meta-box'));
require_once RWMB_DIR . 'meta-box.php';
require_once($template_directory.'/functions/meta-box_settings.php');
/* Menu and it's custom markup */
require_once($template_directory.'/functions/menu.php');
/* Comments custom markup */
require_once($template_directory.'/functions/comments.php');
/* wp_link_pages both next and numbers usage */
require_once($template_directory.'/functions/link_pages.php');
/* Sidebars init */
require_once($template_directory.'/functions/sidebars.php');
/* Sidebar generator */
require_once($template_directory.'/vendor/sidebar_generator.php');
/* Plugins activation */
require_once($template_directory.'/functions/plugin_activation.php');
/* CSS and JS enqueue */
require_once($template_directory.'/functions/enqueue.php');
/* Widgets */
if (file_exists(get_stylesheet_directory().'/functions/widgets/contact.php')) {
	require_once(get_stylesheet_directory().'/functions/widgets/contact.php');
} else {
	require_once($template_directory.'/functions/widgets/contact.php');
}
if (file_exists(get_stylesheet_directory().'/functions/widgets/socials.php')) {
	require_once(get_stylesheet_directory().'/functions/widgets/socials.php');
} else {
	require_once($template_directory.'/functions/widgets/socials.php');
}
if (file_exists(get_stylesheet_directory().'/functions/widgets/login.php')) {
	require_once(get_stylesheet_directory().'/functions/widgets/login.php');
} else {
	require_once($template_directory.'/functions/widgets/login.php');
}
add_filter('widget_text', 'do_shortcode');
/* Demo Import */
require_once($template_directory.'/functions/demo_import.php');
/* Auto Updater */
require_once($template_directory.'/vendor/tf_updater/index.php');

require_once($template_directory.'/functions/ajax_grid_blog.php');
require_once($template_directory.'/functions/ajax_blog.php');
require_once($template_directory.'/functions/ajax_portfolio.php');
require_once($template_directory.'/functions/ajax_import.php');
require_once($template_directory.'/functions/ajax_contact.php');

/* Visual Composer Config */
require_once($template_directory.'/functions/vc_config.php');
/* WooCommerce */
require_once($template_directory.'/functions/woocommerce.php');
/* bbPress */
require_once($template_directory.'/functions/bbpress.php');

/**
 * Theme Setup
 */
function us_theme_setup()
{
	global $smof_data, $content_width;

	if ( ! isset( $content_width ) ) $content_width = 1500;
	add_theme_support('automatic-feed-links');

	add_theme_support('post-formats', array('quote', 'image', 'gallery', 'video', ));

	/* Add post thumbnail functionality */
add_theme_support('post-thumbnails');
add_image_size('portfolio-list', 600, 400, true);
add_image_size('portfolio-list-3-2', 600, 400, true);
add_image_size('portfolio-list-4-3', 600, 450, true);
add_image_size('portfolio-list-1-1', 600, 600, true);
add_image_size('portfolio-list-2-3', 400, 600, true);
add_image_size('portfolio-list-3-4', 450, 600, true);
add_image_size('member', 350, 350, true);
add_image_size('blog-small', 350, 350, true);
add_image_size('blog-grid', 600, 0, false);
add_image_size('blog-large', 1140, 600, true);
add_image_size('gallery-s', 150, 150, true);
add_image_size('gallery-m', 350, 350, true);
add_image_size('gallery-l', 600, 600, true);
add_image_size('gallery-masonry-s', 150, 0, false);
add_image_size('gallery-masonry-m', 600, 0, false);
add_image_size('gallery-masonry-l', 600, 0, false);
add_image_size('carousel-thumb', 600, 400, false);


	/* hide admin bar */
//	show_admin_bar( false );

	/* Excerpt length */
	if (isset($smof_data['blog_excerpt_length']) AND $smof_data['blog_excerpt_length'] != 55) {
		add_filter( 'excerpt_length', 'us_excerpt_length', 999 );
	}

	/* Remove [...] from excerpt */
	add_filter('excerpt_more', 'us_excerpt_more');

	/* Theme localization */
	load_theme_textdomain( 'us', get_template_directory() . '/languages' );
}

add_action( 'after_setup_theme', 'us_theme_setup' );

if (function_exists('set_revslider_as_theme')) {
	set_revslider_as_theme();
}

if (function_exists('vc_set_as_theme')) {
	vc_set_as_theme(true);
}

define('ULTIMATE_NO_EDIT_PAGE_NOTICE', true);
define('ULTIMATE_NO_PLUGIN_PAGE_NOTICE', true);

define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);

function us_excerpt_length( $length ) {
	global $smof_data;
	return $smof_data['blog_excerpt_length'];
}

function us_excerpt_more( $more ) {
	return '...';
}

if ( ! function_exists('us_wp_title')) {
	function us_wp_title( $title ) {
		if (is_front_page()) {
			return get_bloginfo('name') . ' - ' . get_bloginfo('description');
		} else {
			return trim($title) . ' - ' . get_bloginfo('name');
		}
	}
	add_filter( 'wp_title', 'us_wp_title' );
}

/* Custom code goes below this line. */

/* Custom code goes above this line. */


//************************** Custom review (Learn page) shortcode add by rohit kaushal ****************
/*
function reviewfun_play() {
		$args = array(
			'post_type' 	=> 'customer-review',
			'showposts'		=> '4',
			'orderby'    	=> 'date',
			'tax_query' 	=> array(
				array(
					'taxonomy' => 'reviews',
					'field'    => 'slug',
					'terms'    => 'play',
					
				),
			),
		);
		$new = new WP_Query( $args );
		//$new = new WP_Query('post_type=customer-review&showposts=4&orderby=date');
		while ($new->have_posts()) : $new->the_post();
			$postid 	= 	$new->post->ID
		?>
						
			<div class="clientreviews">

				<?php
				
				$ratingval		=	 get_field('rating',$postid);
				$rating			=	(!empty($ratingval))?$ratingval:'0';
				?>
				<div class="stars">
				<?php	
				for($i=1,$j=5;$i<=$rating;$i++,$j--){ ?>
					<span class="star full"><i class="fa fa-star"></i></span>
				<?php	
				}
				for($m=1;$m<=$j;$m++){ ?>
					<span class="star"><i class="fa fa-star-o"></i></span>
				<?php	
				}
				?>
				<br/>
				<b><?php the_title(); ?></b>
                <?php
				if(get_field('post_by',$postid))
				{
				?>
					<p> <b>By <?php echo get_field('post_by',$postid);?>
					<?php
						if(get_field('show_date',$postid)){
							echo ', '.date('F j, Y',strtotime($new->post->post_date)).'</b></p>';
						} else {
							echo '</b></p>';
						}		
				}
				?>
				</div>
				
				<?php echo substr($new->post->post_content,0,1500)?> 
				<p></p>
			
			</div><!-- single-post-wrapper -->
	<?php 
	endwhile; 
	
}


add_shortcode('review_section_play','reviewfun_play');

//************************** Custom review (read)shortcode add by rohit kaushal ****************

function reviewfun_read() {
		$args = array(
			'post_type' 	=> 'customer-review',
			'showposts'		=> '4',
			'orderby'    	=> 'date',
			'tax_query' 	=> array(
				array(
					'taxonomy' => 'reviews',
					'field'    => 'slug',
					'terms'    => 'read',
					
				),
			),
		);
		$new = new WP_Query( $args );
		while ($new->have_posts()) : $new->the_post();
			$postid 	= 	$new->post->ID;
		?>
						
			<div class="clientreviews">

				<?php
				
				$ratingval		=	 get_field('rating',$postid);
				$rating			=	(!empty($ratingval))?$ratingval:'0';
				?>
				<div class="stars">
				<?php	
				for($i=1,$j=5;$i<=$rating;$i++,$j--){ ?>
					<span class="star full"><i class="fa fa-star"></i></span>
				<?php	
				}
				for($m=1;$m<=$j;$m++){ ?>
					<span class="star"><i class="fa fa-star-o"></i></span>
				<?php	
				}
				?>
				<br/>
				<b><?php the_title(); ?></b>
                <?php
				if(get_field('post_by',$postid))
				{
				?>
					<p> <b>By <?php echo get_field('post_by',$postid);?>
					<?php
						if(get_field('show_date',$postid)){
							echo ', '.date('F j, Y',strtotime($new->post->post_date)).'</b></p>';
						} else {
							echo '</b></p>';
						}	
				}
				?>
				</div>
				
				<?php echo substr($new->post->post_content,0,1500)?> 
				<p></p>
			
			</div><!-- single-post-wrapper -->
	<?php 
	endwhile; 
	
}


add_shortcode('review_section_read','reviewfun_read');
*/ 
function cafepress_function() { ?>
<script type='text/javascript'>
var CPwaitForLoad,jQwaitForLoad,init=function(){if("CPWidget"in window)window.clearTimeout(CPwaitForLoad),CPWidget.init({title:"My CafePress Products",topcolor:"#8CBE21",bottomcolor:"#79A618",bgcolor:"#FFFFFF",rows:1,cols:3,imgsize:"200",prodtitle:"false",partnerid:"",shopid:"",tags:""});else{CPwaitForLoad=window.setTimeout(init,200);var t=document.createElement("SCRIPT");t.src=" <?php echo get_template_directory_uri(); ?>/js/cpwidget.js",t.type="text/javascript",document.getElementsByTagName("head")[0].appendChild(t)}},jQinit=function(){if("jQuery"in window)window.clearTimeout(jQwaitForLoad),init();else{var t=document.createElement("SCRIPT");t.src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js",t.type="text/javascript",document.getElementsByTagName("head")[0].appendChild(t),jQwaitForLoad=window.setTimeout(jQinit,200)}};jQinit();</script>

<div id='cpwidget'></div>	
<?php
}


add_shortcode('cafepress_widget','cafepress_function');


//************************ Single product short code display on read page  ************************//

function product_fun($atts) {
	$product_id	=	4673; // Default for Diwali Gift
	if(!empty($atts) && !empty($atts['product_id'])){
		$product_id	=	$atts['product_id'];
	}
	
	$args 			= 	array(
								'post_type' => 'product',
								'p'	=>$product_id
													
							);
	query_posts( $args );
	while ( have_posts() ) : the_post();
	$postid 			= 	get_the_ID();
	$post_rec 			= 	get_post($postid);
	$regular_price_arr 	=	get_post_meta( get_the_ID(), '_regular_price' );
	$sale_price_arr 	=	get_post_meta( get_the_ID(), '_sale_price' );
	$actualprice		=	'';
	$regular_price      =   '';
	if(!empty($regular_price_arr[0]) && !empty($sale_price_arr[0])) :
		$actualprice 	=	 $sale_price_arr[0];
		$regular_price	=	$regular_price_arr[0];
	elseif(!empty($sale_price_arr[0])) :
		$actualprice	= 	$sale_price_arr[0];
		$regular_price	=    $regular_price_arr[0];
	else:
		$actualprice	= 	$regular_price_arr[0];
		$regular_price	=	'';
	endif;
?>	
	<div class="wpb_text_column product-right-info">
		<div class="wpb_wrapper">
			
			<div class="product-title">
			<h1 class="text-orange"><?php the_title();?></h1>
			<?php the_excerpt(); ?>
			</div>
					
			<div class="product-price clearfix">
			<?php if(!empty($regular_price)) : ?>	
			<div class="price disable-price"><sup class="currency">$</sup><span class="figure"><?php echo $regular_price ; ?></span></div>	
			<?php endif; ?>	
			<?php if(!empty($actualprice)) : ?>	
			<div class="price"><sup class="currency">$</sup><span class="figure"><?php echo $actualprice ;?></span></div>
			<?php endif ;?>
			<div class="price"></div>
			</div>
			<p><?php echo $post_rec->post_content;?></p>
			<p></p>
			<?php if(!empty($actualprice)) : ?>
				<?php if($product_id == 4673) : // remove Buy Now button till client confirmation ?>
				<a class="btn btn-orange button add_to_cart_button product_type_simple" href="<?php echo home_url();?>/shop/?add-to-cart=<?php echo $postid ;?>" rel="nofollow" data-quantity="1" data-product_sku="" data-product_id="<?php echo $postid ;?>">Buy Now</a>
				<?php endif ; ?>
			<?php endif ;?>
		</div>
	</div>
<?php	
	endwhile;
	// Reset Query
	wp_reset_query();
}

add_shortcode('custom_product','product_fun');

//*********************  End of single product short code *****************************************// 

//************************** Custom reviews shortcode added by Harjeet Singh ****************

function reviewsFun($atts) {
		$term	=	'read'; // Default for Diwali Gift
		if(!empty($atts) && !empty($atts['term'])){
			$term	=	$atts['term'];
		}
		
		$args = array(
			'post_type' 	=> 'customer-review',
			'showposts'		=> '4',
			'orderby'    	=> 'date',
			'tax_query' 	=> array(
				array(
					'taxonomy' => 'reviews',
					'field'    => 'slug',
					'terms'    => $term,
					
				),
			),
		);
		$new = new WP_Query( $args );
		while ($new->have_posts()) : $new->the_post();
			$postid 	= 	$new->post->ID;
		?>
						
			<div class="clientreviews">

				<?php
				
				$ratingval		=	 get_field('rating',$postid);
				$rating			=	(!empty($ratingval))?$ratingval:'0';
				?>
				<div class="stars">
				<?php	
				for($i=1,$j=5;$i<=$rating;$i++,$j--){ ?>
					<span class="star full"><i class="fa fa-star"></i></span>
				<?php	
				}
				for($m=1;$m<=$j;$m++){ ?>
					<span class="star"><i class="fa fa-star-o"></i></span>
				<?php	
				}
				?>
				<br/>
				<b><?php the_title(); ?></b>
                <?php
				if(get_field('post_by',$postid))
				{
				?>
					<p> <b>By <?php echo get_field('post_by',$postid);?>
					<?php
						if(get_field('show_date',$postid)){
							echo ', '.date('F j, Y',strtotime($new->post->post_date)).'</b></p>';
						} else {
							echo '</b></p>';
						}	
				}
				?>
				</div>
				
				<?php echo substr($new->post->post_content,0,1500)?> 
				<p></p>
			
			</div><!-- single-post-wrapper -->
	<?php 
	endwhile; 
	
}


add_shortcode('reviews','reviewsFun');
// ----------------------------------------------------------------------------------------------------------

 // Restrict to only one product in cart
add_filter( 'woocommerce_add_to_cart_validation', 'only_one_in_cart' );
 
function only_one_in_cart( $cart_item_data ) {
global $woocommerce;
$woocommerce->cart->empty_cart();
return $cart_item_data;
}


