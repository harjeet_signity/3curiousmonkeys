<?php
/*
Plugin Name: Elastic Email
Description: This plugin reconfigures the wp_mail() function to send email using HTTPS (via Elasticemail API) instead of SMTP and creates an options page that allows you to specify various options.
Version: 1.0
Author: esideout
Author URI: http://esideout.com
Plugin URI: http://esideout.com
*/

// Configuration options names:

define( 'EM_SETTINGS_GROUP', 'em_settings_group' );
define( 'EM_ENABLED', 'em_enabled' );
define( 'EM_USERNAME', 'em_username' );
define( 'EM_APIKEY', 'em_apikey' );
define( 'EM_URL', 'em_url' );
define( 'EM_CHANNEL', 'em_channel' );

define( 'EM_PORT', 443 );
define( 'EM_HEADER_SEPARATOR', "\r\n" );
define( 'EM_TIMEOUT', 10 );
define( 'EM_READ_LENGTH', 1024 );
define( 'EM_EMAIL_SEPARATOR', ',' );

// Banner configuration

define( 'EM_BANNER', 'http://www.esideout.com/images/esideout-unix-technology-728x90-leaderboard.gif' );
define( 'EM_BANNER_LINK', 'http://www.esideout.com' );
define( 'EM_BANNER_ALT', 'Visit esideout.com' );


if ( is_admin() ) {
    add_action( 'admin_menu', 'em_admin_menu' );
}

function em_admin_menu() {
    add_options_page( 'Elastic Mail API', 'Elastic Email API', 'administrator', __FILE__, 'em_settings_page', '', __FILE__ );
}

function em_settings_page() {
    if ( !empty( $_POST ) ) {
        if ( isset( $_POST[ EM_ENABLED ] ) ) {
            update_option( EM_ENABLED, $_POST[ EM_ENABLED ] );
        } else {
            update_option ( EM_ENABLED, 0 );
        }
        if ( isset( $_POST[ EM_USERNAME ] ) ) {
            update_option( EM_USERNAME, $_POST[ EM_USERNAME ] );
        }
        if ( isset( $_POST[ EM_APIKEY ] ) ) {
            update_option( EM_APIKEY, $_POST[ EM_APIKEY ] );
        }
        if ( isset( $_POST[ EM_URL ] ) ) {
            update_option( EM_URL, $_POST[ EM_URL ] );
        }
        if ( isset( $_POST[ EM_CHANNEL ] ) ) {
            update_option( EM_CHANNEL, $_POST[ EM_CHANNEL ] );
        }

        $message = 'Options have been successfully saved.';
    }
?>
<div class="wrap">
    <h2>Elastic Mail Settings</h2>

    <?php if ( !empty( $message ) ): ?>
    <p><strong><?php echo $message ?></strong></p>
    <?php endif; ?>

    <form method="post" action="">

        <?php settings_fields( EM_SETTINGS_GROUP ); ?>
        <?php $enable_checked = get_option( EM_ENABLED ) ? ' checked="checked"' : ''; ?>

        <table class="form-table">
            <tr valign="top">
                <th scope="row">Enabled</th>
                <td><input type="checkbox" name="<?php echo EM_ENABLED ?>" value="1"<?php echo $enable_checked ?> /></td>
                <td><small>Enabled or disable Elastic mail API.</small></td>
            </tr>

            <tr valign="top">
                <th scope="row">API Username</th>
                <td><input type="text" name="<?php echo EM_USERNAME ?>" value="<?php echo get_option( EM_USERNAME ) ?>" size="32" /></td>
                <td><small>Elastic API username (can't be empty).</small></td>
            </tr>

            <tr valign="top">
                <th scope="row">API Key</th>
                <td><input type="text" name="<?php echo EM_APIKEY ?>" value="<?php echo get_option( EM_APIKEY ) ?>" size="32" /></td>
                <td><small>Elastic API Key (can't be empty).</small></td>
            </tr>

            <tr valign="top">
                <th scope="row">API URL</th>
                <td><input type="text" name="<?php echo EM_URL ?>" value="<?php echo get_option( EM_URL ) ?>" size="32" /></td>
                <td><small>Elastic API URL (can't be empty, default: ssl://api.elasticemail.com/mailer/send).</small></td>
            </tr>

            <tr valign="top">
                <th scope="row">API Channel</th>
                <td><input type="text" name="<?php echo EM_CHANNEL ?>" value="<?php echo get_option( EM_CHANNEL ) ?>" size="32" /></td>
                <td><small>Elastic API channel (can be empty).</small></td>
            </tr>

        </table>

        <p class="submit">
            <input type="submit" class="button-primary" value="<?php _e('Save Settings') ?>" />
        </p>
    
    </form>

    <p>
        <a href="<?php echo EM_BANNER_LINK ?>" title="<?php echo EM_BANNER_ALT ?>">
            <img src="<?php echo EM_BANNER ?>" alt="<?php echo EM_BANNER_ALT ?>" />
        </a>
    </p>

</div>
<?php
}

function em_enabled_configured() {
    $username = get_option( EM_USERNAME );
    $apikey = get_option( EM_APIKEY );
    return get_option( EM_ENABLED ) && !empty($username) && !empty($apikey);
}

function em_send( &$phpmailer, $to ) {
    $return = $errno = $errstr = NULL;

    $channel = get_option( EM_CHANNEL );

    if ( is_array( $to ) ) {
        $to = implode( EM_EMAIL_SEPARATOR, $to );
    }

    $data = 'username=' . urlencode( get_option( EM_USERNAME ) );
    $data .= '&api_key=' . urlencode( get_option( EM_APIKEY ) );
    $data .= '&from=' . urlencode( $phpmailer->From );
    $data .= '&from_name=' . urlencode( $phpmailer->FromName );
    $data .= '&to=' . urlencode( $to );
    $data .= '&subject=' . urlencode( $phpmailer->Subject );

    if ( $phpmailer->ContentType == 'text/html' ) {
         $data .= '&body_html=' . urlencode( $phpmailer->Body );
         $data .= '&body_text=' . urlencode( strip_tags( $phpmailer->Body ) );
    } else {
         $data .= '&body_text=' . urlencode( $phpmailer->Body );
    }
    
    if ( !empty( $channel ) ) {
        $data .= '&channel=' . urlencode( $channel );
    }
    
    $url = get_option( EM_URL );
    $url_data = parse_url( $url );

    $hostname = $url_data['scheme'] . '://' . $url_data['host'];
    
    $headers = 'POST ' . $url_data['path'] . ' HTTP/1.0' . EM_HEADER_SEPARATOR;
    $headers .= 'Content-Type: application/x-www-form-urlencoded' . EM_HEADER_SEPARATOR;
    $headers .= 'Content-Length: ' . strlen($data) . EM_HEADER_SEPARATOR . EM_HEADER_SEPARATOR;

    $fp = fsockopen( $hostname, EM_PORT, $errno, $errstr, EM_TIMEOUT);

    if ( !$fp ) {
        return FALSE;
    } else {
        fputs( $fp, $headers . $data );

        while ( !feof( $fp ) ) {
            $return .= fread ( $fp, EM_READ_LENGTH );
        }

        fclose( $fp );
    }

    return ( substr_count( $return, '200 OK' ) > 0 );
}

if ( em_enabled_configured() && !function_exists('wp_mail') ) {

/**
 * This was wp_mail() funtion from Wordpress 3.2.1
 *
 * @param string|array $to Array or comma-separated list of email addresses to send message.
 * @param string $subject Email subject
 * @param string $message Message contents
 * @param string|array $headers Optional. Additional headers.
 * @param string|array $attachments Optional. Files to attach.
 * @return bool Whether the email contents were sent successfully.
 */
function wp_mail( $to, $subject, $message, $headers = '', $attachments = array() ) {
    // Compact the input, apply the filters, and extract them back out
    extract( apply_filters( 'wp_mail', compact( 'to', 'subject', 'message', 'headers', 'attachments' ) ) );

    // Doesn't support attachments
    if ( !empty($attachments) ) {
        return FALSE;
    }
    
    if ( !is_array($attachments) )
        $attachments = explode( "\n", str_replace( "\r\n", "\n", $attachments ) );

    global $phpmailer;

    // (Re)create it, if it's gone missing
    if ( !is_object( $phpmailer ) || !is_a( $phpmailer, 'PHPMailer' ) ) {
        require_once ABSPATH . WPINC . '/class-phpmailer.php';
        require_once ABSPATH . WPINC . '/class-smtp.php';
        $phpmailer = new PHPMailer( true );
    }

    // Headers
    if ( empty( $headers ) ) {
        $headers = array();
    } else {
        if ( !is_array( $headers ) ) {
            // Explode the headers out, so this function can take both
            // string headers and an array of headers.
            $tempheaders = explode( "\n", str_replace( "\r\n", "\n", $headers ) );
        } else {
            $tempheaders = $headers;
        }
        $headers = array();
        $cc = array();
        $bcc = array();

        // If it's actually got contents
        if ( !empty( $tempheaders ) ) {
            // Iterate through the raw headers
            foreach ( (array) $tempheaders as $header ) {
                if ( strpos($header, ':') === false ) {
                    if ( false !== stripos( $header, 'boundary=' ) ) {
                        $parts = preg_split('/boundary=/i', trim( $header ) );
                        $boundary = trim( str_replace( array( "'", '"' ), '', $parts[1] ) );
                    }
                    continue;
                }
                // Explode them out
                list( $name, $content ) = explode( ':', trim( $header ), 2 );

                // Cleanup crew
                $name    = trim( $name    );
                $content = trim( $content );

                switch ( strtolower( $name ) ) {
                    // Mainly for legacy -- process a From: header if it's there
                    case 'from':
                        if ( strpos($content, '<' ) !== false ) {
                            // So... making my life hard again?
                            $from_name = substr( $content, 0, strpos( $content, '<' ) - 1 );
                            $from_name = str_replace( '"', '', $from_name );
                            $from_name = trim( $from_name );

                            $from_email = substr( $content, strpos( $content, '<' ) + 1 );
                            $from_email = str_replace( '>', '', $from_email );
                            $from_email = trim( $from_email );
                        } else {
                            $from_email = trim( $content );
                        }
                        break;
                    case 'content-type':
                        if ( strpos( $content, ';' ) !== false ) {
                            list( $type, $charset ) = explode( ';', $content );
                            $content_type = trim( $type );
                            if ( false !== stripos( $charset, 'charset=' ) ) {
                                $charset = trim( str_replace( array( 'charset=', '"' ), '', $charset ) );
                            } elseif ( false !== stripos( $charset, 'boundary=' ) ) {
                                $boundary = trim( str_replace( array( 'BOUNDARY=', 'boundary=', '"' ), '', $charset ) );
                                $charset = '';
                            }
                        } else {
                            $content_type = trim( $content );
                        }
                        break;
                    case 'cc':
                        $cc = array_merge( (array) $cc, explode( ',', $content ) );
                        break;
                    case 'bcc':
                        $bcc = array_merge( (array) $bcc, explode( ',', $content ) );
                        break;
                    default:
                        // Add it to our grand headers array
                        $headers[trim( $name )] = trim( $content );
                        break;
                }
            }
        }
    }

    // Empty out the values that may be set
    $phpmailer->ClearAddresses();
    $phpmailer->ClearAllRecipients();
    $phpmailer->ClearAttachments();
    $phpmailer->ClearBCCs();
    $phpmailer->ClearCCs();
    $phpmailer->ClearCustomHeaders();
    $phpmailer->ClearReplyTos();

    // From email and name
    // If we don't have a name from the input headers
    if ( !isset( $from_name ) )
        $from_name = 'WordPress';

    /* If we don't have an email from the input headers default to wordpress@$sitename
     * Some hosts will block outgoing mail from this address if it doesn't exist but
     * there's no easy alternative. Defaulting to admin_email might appear to be another
     * option but some hosts may refuse to relay mail from an unknown domain. See
     * http://trac.wordpress.org/ticket/5007.
     */

    if ( !isset( $from_email ) ) {
        // Get the site domain and get rid of www.
        $sitename = strtolower( $_SERVER['SERVER_NAME'] );
        if ( substr( $sitename, 0, 4 ) == 'www.' ) {
            $sitename = substr( $sitename, 4 );
        }

        $from_email = 'wordpress@' . $sitename;
    }

    // Plugin authors can override the potentially troublesome default
    $phpmailer->From     = apply_filters( 'wp_mail_from'     , $from_email );
    $phpmailer->FromName = apply_filters( 'wp_mail_from_name', $from_name  );

    // Set destination addresses
    if ( !is_array( $to ) )
        $to = explode( ',', $to );

    foreach ( (array) $to as $recipient ) {
        try {
            // Break $recipient into name and address parts if in the format "Foo <bar@baz.com>"
            $recipient_name = '';
            if( preg_match( '/(.+)\s?<(.+)>/', $recipient, $matches ) ) {
                if ( count( $matches ) == 3 ) {
                    $recipient_name = $matches[1];
                    $recipient = $matches[2];
                }
            }
            $phpmailer->AddAddress( trim( $recipient ), $recipient_name);
        } catch ( phpmailerException $e ) {
            continue;
        }
    }

    // Set mail's subject and body
    $phpmailer->Subject = $subject;
    $phpmailer->Body    = $message;

    // Add any CC and BCC recipients
    if ( !empty( $cc ) ) {
        foreach ( (array) $cc as $recipient ) {
            try {
                // Break $recipient into name and address parts if in the format "Foo <bar@baz.com>"
                $recipient_name = '';
                if( preg_match( '/(.+)\s?<(.+)>/', $recipient, $matches ) ) {
                    if ( count( $matches ) == 3 ) {
                        $recipient_name = $matches[1];
                        $recipient = $matches[2];
                    }
                }
                $phpmailer->AddCc( trim($recipient), $recipient_name );
            } catch ( phpmailerException $e ) {
                continue;
            }
        }
    }

    if ( !empty( $bcc ) ) {
        foreach ( (array) $bcc as $recipient) {
            try {
                // Break $recipient into name and address parts if in the format "Foo <bar@baz.com>"
                $recipient_name = '';
                if( preg_match( '/(.+)\s?<(.+)>/', $recipient, $matches ) ) {
                    if ( count( $matches ) == 3 ) {
                        $recipient_name = $matches[1];
                        $recipient = $matches[2];
                    }
                }
                $phpmailer->AddBcc( trim($recipient), $recipient_name );
            } catch ( phpmailerException $e ) {
                continue;
            }
        }
    }

    // Set to use PHP's mail()
    $phpmailer->IsMail();

    // Set Content-Type and charset
    // If we don't have a content-type from the input headers
    if ( !isset( $content_type ) )
        $content_type = 'text/plain';

    $content_type = apply_filters( 'wp_mail_content_type', $content_type );

    $phpmailer->ContentType = $content_type;

    // Set whether it's plaintext, depending on $content_type
    if ( 'text/html' == $content_type )
        $phpmailer->IsHTML( true );

    // If we don't have a charset from the input headers
    if ( !isset( $charset ) )
        $charset = get_bloginfo( 'charset' );

    // Set the content-type and charset
    $phpmailer->CharSet = apply_filters( 'wp_mail_charset', $charset );

    // Set custom headers
    if ( !empty( $headers ) ) {
        foreach( (array) $headers as $name => $content ) {
            $phpmailer->AddCustomHeader( sprintf( '%1$s: %2$s', $name, $content ) );
        }

        if ( false !== stripos( $content_type, 'multipart' ) && ! empty($boundary) )
            $phpmailer->AddCustomHeader( sprintf( "Content-Type: %s;\n\t boundary=\"%s\"", $content_type, $boundary ) );
    }

    if ( !empty( $attachments ) ) {
        foreach ( $attachments as $attachment ) {
            try {
                $phpmailer->AddAttachment($attachment);
            } catch ( phpmailerException $e ) {
                continue;
            }
        }
    }

    do_action_ref_array( 'phpmailer_init', array( &$phpmailer ) );

    // Send!
    try {
        //$phpmailer->Send();
        return em_send( $phpmailer, $to );
    } catch ( phpmailerException $e ) {
        return false;
    }

}
}
