<?php

$to = 'harjeet.s@signitysolutions.in';

// subject
$subject = 'Email Functionality Testing';

// message
$message = '
<html>
<head>
  <title>Testing  Server for mail</title>
</head>
<body>
  <p>Here are the birthdays upcoming in August!</p>
  <table>
    <tr>
      <th>Person</th><th>Day</th><th>Month</th><th>Year</th>
    </tr>
    <tr>
      <td>Joe</td><td>3rd</td><td>August</td><td>1970</td>
    </tr>
    <tr>
      <td>Sally</td><td>17th</td><td>August</td><td>1973</td>
    </tr>
  </table>
</body>
</html>
';

// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Additional headers
//$headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
$headers .= 'From: Birthday Reminder <harjeet.s@signitysolutions.in>' . "\r\n";


// Mail it
if(mail($to, $subject, $message, $headers)){
	echo "Mail sent successfully";
} else {
	echo "Mail not sent successfully";	
}
?>
